const fp = require('lodash/fp');

const cars = [
    {name : 'Ferrari FF', horsepower: 660, dollar_value: 700000, in_stock: true},
    {name : 'Spyker C12 Zagato', horsepower: 650, dollar_value: 648000, in_stock: false},
    {name : 'Jaguar XKR-S', horsepower: 550, dollar_value: 132000, in_stock: false},
    {name : 'Audi R8', horsepower: 525, dollar_value: 114200, in_stock: false},
    {name : 'Aston Martin One-77', horsepower: 750, dollar_value: 1850000, in_stock: true},
    {name : 'Pagani Huayra', horsepower: 700, dollar_value: 1300000, in_stock: false}
]


// (1) 使用函数组合 fp.flowRight() 重新实现下面这个函数
/*let isLastInStock = function (cars) {
    let last_car = fp.last(cars);
    return fp.prop('in_stock', last_car);
};*/


//console.log('isLastInStock', isLastInStock(cars));

// fp.flowRight()
const isLastInStock = fp.flowRight(fp.prop('in_stock'), fp.last);
console.log('isLastInStock with compose fp.flowRight() : ', isLastInStock(cars));

// (2) 使用 fp.flowRight(), fp.prop(), fp.first() 获取第一个 car 的 name
const getFirstCarName = fp.flowRight(fp.prop('name'), fp.first);
console.log('getFirstCarName :', getFirstCarName(cars));

// (3) 使用帮助函数 _average 重构 averageDollarValue, 使用函数组合的方式实现
let _average = function (xs) {
    return fp.reduce(fp.add, 0, xs);
};
let averageDollarValue = function (cars) {
    let dollar_value = fp.map(function (car) {
        return car.dollar_value;
    }, cars)
    return _average(dollar_value);
}

// 重构后的代码
let averageDollarValueWithRefactor = fp.flowRight(_average, fp.map(fp.prop('dollar_value')));

console.log('test with averageDollarValue : ', averageDollarValue(cars));
console.log('result with averageDollarValueWithRefactor : ', averageDollarValueWithRefactor(cars));

// (4) 使用 flowRight 写一个 sanitizeNames() 函数, 返回一个下划线连接的小写字符串，把数组中的 name 转换成这种形式，例如:
// sanitizeNames(['Hello World']) => ['hello_world']

let _underscore = fp.replace(/\W+/g, '_');
const sanitizeNames = fp.flowRight(fp.map(_underscore), fp.map(fp.toLower), fp.map(car=>car.name))
console.log('sanitizeNames :', sanitizeNames(cars));

