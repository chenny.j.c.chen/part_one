const PENDING = "pending";//等待
const FULFILLED = "fulfilled";//成功
const REJECTED = "rejected"
class MyPromise {
    constructor(executor) {
        try {
            executor(this.resolve, this.reject)
        } catch (e) {
            this.reject(e)
        }
    }
    //promise 状态
    status = PENDING;
    //成功之后的值
    value = undefined;
    //失败之后的原因
    reason = undefined;
    //成功的回调函数  多个then
    successCallBack = [];
    //失败的回调函数
    failCallBack = [];
    resolve = value => {
        //如果状态不是等待 阻止程序向下执行
        if (this.status !== PENDING) return;
        //将状态改成成功
        this.status = FULFILLED;
        this.value = value;
        //while (this.successCallBack.length) this.successCallBack.shift()(this.value);
        while (this.successCallBack.length) this.successCallBack.shift()();

    }
    reject = reason => {
        //如果状态不是等待 阻止程序向下执行
        if (this.status !== PENDING) return;
        //将状态改成失败
        this.status = REJECTED;
        this.reason = reason;
        // while (this.failCallBack.length) this.failCallBack.shift()(this.reason);
        while (this.failCallBack.length) this.failCallBack.shift()();

    }
    then (successCallBack, failCallBack) {
        successCallBack = successCallBack ? successCallBack : value => value;
        failCallBack = failCallBack ? failCallBack : reason => { throw reason }
        let promise2 = new MyPromise((resolve, mReject) => {
            //判断状态
            if (this.status === FULFILLED) {
                //为了拿到promise2把引用promise2的地方设置为异步
                setTimeout(() => {
                    try {
                        let x = successCallBack(this.value);
                        /**
                         * 判断x的值是普通值还是promise对象
                         * 如果是普通值 直接调用resolve
                         * 如果是promise对象查看promise对象返回的结果
                         * 在根据promise对象返回的结果 决定调用resolve还是调用reject
                         */
                        resolvePromise(promise2, x, resolve, mReject);
                    } catch (e) {
                        mReject(e)
                    }
                }, 0)
                // resolve(x)
            } else if (this.status === REJECTED) {
                //为了拿到promise2把引用promise2的地方设置为异步
                setTimeout(() => {
                    try {
                        let x = failCallBack(this.reason);

                        /**
                         * 判断x的值是普通值还是promise对象
                         * 如果是普通值 直接调用resolve
                         * 如果是promise对象查看promise对象返回的结果
                         * 在根据promise对象返回的结果 决定调用resolve还是调用reject
                         */
                        resolvePromise(promise2, x, resolve, mReject);
                    } catch (e) {
                        mReject(e)
                    }
                }, 0)
            } else {
                //如果是pending的状态，就保存两个回调函数
                this.successCallBack.push(() => {
                    setTimeout(() => {
                        try {
                            let x = successCallBack(this.value);
                            /**
                             * 判断x的值是普通值还是promise对象
                             * 如果是普通值 直接调用resolve
                             * 如果是promise对象查看promise对象返回的结果
                             * 在根据promise对象返回的结果 决定调用resolve还是调用reject
                             */
                            resolvePromise(promise2, x, resolve, mReject);
                        } catch (e) {
                            mReject(e)
                        }
                    }, 0)
                });
                this.failCallBack.push(() => {
                    //为了拿到promise2把引用promise2的地方设置为异步
                    setTimeout(() => {
                        try {
                            let x = failCallBack(this.reason);

                            /**
                             * 判断x的值是普通值还是promise对象
                             * 如果是普通值 直接调用resolve
                             * 如果是promise对象查看promise对象返回的结果
                             * 在根据promise对象返回的结果 决定调用resolve还是调用reject
                             */
                            resolvePromise(promise2, x, resolve, mReject);
                        } catch (e) {
                            mReject(e)
                        }
                    }, 0)
                });
            }
        })

        return promise2;
    }
    finally (callback) {
        return this.then(value => {
            return MyPromise.resolve(callback()).then(() => value);
        }, reason => {
            return MyPromise.resolve(callback()).then(() => { throw reason });
        })
    }
    catch (failCallBack) {
        return this.then(undefined, failCallBack);
    }
    static all (array) {
        let result = [];
        let index = 0;
        return new MyPromise((resolve, reject) => {
            function addData (key, value) {
                result[key] = value;
                index++
                if (index === array.length) {
                    mResolve(result)
                }
            }
            for (let i = 0; i < array.length; i++) {
                let current = array[i];
                if (current instanceof MyPromise) {
                    //promise对象
                    current.then(value => {
                        addData(i, value)
                    }, reason => {
                        reject(reason)
                    })
                } else {
                    //普通值
                    addData(i, current);
                }
            }
        })
    }
    static resolve (value) {
        if (value instanceof MyPromise) return value;
        return new MyPromise((resolve, reject) => { resolve(value) });
    }
}
function resolvePromise (promise2, x, resolve, reject) {
    if (promise2 === x) {
        return reject(new TypeError("promise对象不能循环引用"))
    }
    if (x instanceof MyPromise) {
        x.then(resolve, reject);
    } else {
        resolve(x)
    }
}
module.exports = MyPromise;