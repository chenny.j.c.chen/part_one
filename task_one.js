// 将下面异步代码使用 Promise 的方式改进

setTimeout(function () {
    var a = 'hello';
    setTimeout(function () {
        var b = 'lagou';
        setTimeout(function (){
            var c = 'I ❤ u';
            console.log(a + b + c)
        }, 10)
    }, 10)
}, 10)

// 改进后的代码
let promise = new Promise((resolve, reject) => {
    setTimeout(value => {
        resolve(value)
    }, 10);
});

promise.then(value => {
    return 'hello';
    })
    .then(value => {
        return value + 'lagou';
    })
    .then(value => {
        console.log(value + 'I ❤ u')
    });

